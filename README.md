# My Ansible Playbooks

Create a `production` file listing your servers and users to login to:
```
[mastodonbotsservers]
example.com  ansible_user=johndoe
```

Then run the playbook:

```bash
ansible-playbook -i production -l webthingservers site.yml
```
